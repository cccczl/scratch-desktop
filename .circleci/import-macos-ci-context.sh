#!/bin/bash
set -e

# Do not use `set -x` or `set -o xtrace` with this file or it will expose secrets on CI!

function decodeToFile () {
    if [ -z "$1" ]; then
        echo "Missing or invalid filename"
        return 1
    fi
    if [ -z "$2" ]; then
        echo "Missing environment variable contents for file: $1"
        return 2
    fi
    echo "$2" | base64 --decode > "$1"
}

decodeToFile embedded.provisionprofile "${MAC_PROVISION_PROFILE}"
decodeToFile mas-dev.provisionprofile "${MAC_DEV_PROVISION_PROFILE}"
printf "%s" "${CSC_MACOS}" > macos-certs-scratch-foundation.cer
decodeToFile apple-dev-cert.p12 "${MAC_DEV_CERT}"

security -v create-keychain -p circleci circleci.keychain
security -v default-keychain -s circleci.keychain
security -v import macos-certs-scratch-foundation.cer -k circleci.keychain -P "${CSC_MACOS_PASSWORD}" -T /usr/bin/codesign -T /usr/bin/productbuild
security -v import apple-dev-cert.p12 -k circleci.keychain -P "${MAC_DEV_CERT_PASSWORD}" -T /usr/bin/codesign -T /usr/bin/productbuild
security -v unlock-keychain -p circleci circleci.keychain
# "set-key-partition-list" prints extensive not-so-useful output and adding "-q" (even multiple times) doesn't suppress it.
# The "grep -v" at the end of this line suppresses all of that so any errors or warnings might be more visible.
security -v set-key-partition-list -S apple-tool:,apple:,codesign: -s -k circleci circleci.keychain | grep -v '^    0x'
security -v set-keychain-settings -lut 600 circleci.keychain
security -v find-identity circleci.keychain
rm macos-certs-scratch-foundation.cer apple-dev-cert.p12
